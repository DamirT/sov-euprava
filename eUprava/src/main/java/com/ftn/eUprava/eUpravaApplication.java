package com.ftn.eUprava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class eUpravaApplication {

	public static void main(String[] args) {
		SpringApplication.run(eUpravaApplication.class, args);
	}

}
