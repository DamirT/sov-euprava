package com.ftn.eUprava.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class KartonVakcinacije {

	private Long id;
	private Long datumPrijave;
	private Long datumVakcinisanja;
	private Long idVakcine;
	private DozaVakcine dozaVakcine;
	private Long idPacijenta;
	private DateFormat datumFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");

	
	
	public KartonVakcinacije() {
		
	}

	public KartonVakcinacije(Long id, Long datumPrijave, Long datumVakcinisanja, Long idVakcine,
			DozaVakcine dozaVakcine, Long idPacijenta) {
		this.id = id;
		this.datumPrijave = datumPrijave;
		this.datumVakcinisanja = datumVakcinisanja;
		this.idVakcine = idVakcine;
		this.dozaVakcine = dozaVakcine;
		this.idPacijenta = idPacijenta;
	}

	public KartonVakcinacije(Long datumPrijave, Long datumVakcinisanja, Long idVakcine, DozaVakcine dozaVakcine,
			Long idPacijenta) {
		this.datumPrijave = datumPrijave;
		this.datumVakcinisanja = datumVakcinisanja;
		this.idVakcine = idVakcine;
		this.dozaVakcine = dozaVakcine;
		this.idPacijenta = idPacijenta;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDatumPrijave() {
		return datumPrijave;
	}

	public String getDatumPrijaveFormated() {
		return datumFormat.format(new Date(datumPrijave));
	}

	public String getDatumVakcinisanjaFormated() {
		return datumFormat.format(new Date(datumVakcinisanja));
	}

	public Long getDatumVakcinisanja() {
		return datumVakcinisanja;
	}

	public Long getIdVakcine() {
		return idVakcine;
	}

	public DozaVakcine getDozaVakcine() {
		return dozaVakcine;
	}

	public void setDozaVakcine(DozaVakcine dozaVakcine) {
		this.dozaVakcine = dozaVakcine;
	}

	public void setDatumVakcinisanja(Long datumVakcinisanja) {
		this.datumVakcinisanja = datumVakcinisanja;
	}

	public void setIdVakcine(Long idVakcine) {
		this.idVakcine = idVakcine;
	}

	public Long getIdPacijenta() {
		return idPacijenta;
	}

	public boolean isVaccinated() {
		return datumVakcinisanja > 0;
	}

	public void setDatumPrijave(Long datumPrijave) {
		this.datumPrijave = datumPrijave;
	}

}
