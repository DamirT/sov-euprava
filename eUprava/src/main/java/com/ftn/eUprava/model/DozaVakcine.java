package com.ftn.eUprava.model;

public enum DozaVakcine {

	 DOZA_1("Doza 1"),
	 DOZA_2("Doza 2"),
	 DOZA_3("Doza 3");
	
	private String name = "";

	private DozaVakcine(String name) {
		this.name = name;
	}
	
	public static String getName(DozaVakcine dozaVakcine) {
		return dozaVakcine.name;
	}
	
	public static DozaVakcine getDozavakcineByName(String name) {
		for (DozaVakcine dv : values()) {
			if (dv.name.equals(name)) {
				return dv;
			}
		}
		return DOZA_1;
	}

}
