package com.ftn.eUprava.model;

import java.util.List;

public class MedicinskiRadnik extends Korisnik {


	public MedicinskiRadnik(Long id, String ime, String prezime, String jmbg, String lozinka) {
		super(id, ime, prezime, jmbg, lozinka, UlogaKorisnika.MEDICINSKI_RADNIK);
	}
	

}
