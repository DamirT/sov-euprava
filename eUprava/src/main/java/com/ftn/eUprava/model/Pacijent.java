package com.ftn.eUprava.model;

import java.util.List;

public class Pacijent extends Korisnik {

	private List<KartonVakcinacije> kartoniVakcinacije;

	public Pacijent(Long id, String ime, String prezime, String jmbg, String lozinka) {
		super(id, ime, prezime, jmbg, lozinka, UlogaKorisnika.PACIJENT);
	}

	public List<KartonVakcinacije> getKartoniVakcinacije() {
		return kartoniVakcinacije;
	}

	public void setKartoniVakcinacije(List<KartonVakcinacije> kartoniVakcinacije) {
		this.kartoniVakcinacije = kartoniVakcinacije;
	}

	

}
