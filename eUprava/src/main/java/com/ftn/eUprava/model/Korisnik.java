package com.ftn.eUprava.model;

import java.util.Date;

public abstract class Korisnik {

	private Long id;
	private String ime;
	private String prezime;
	private String jmbg;
	private String lozinka;
	private UlogaKorisnika ulogaKorisnika;

	public Korisnik(Long id, String ime, String prezime, String jmbg, String lozinka, UlogaKorisnika ulogaKorisnika) {
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.jmbg = jmbg;
		this.lozinka = lozinka;
		this.ulogaKorisnika = ulogaKorisnika;
	}

	public String getIme() {
		return ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public String getJmbg() {
		return jmbg;
	}

	public String getLozinka() {
		return lozinka;
	}

	public UlogaKorisnika getUlogaKorisnika() {
		return ulogaKorisnika;
	}

	public Long getId() {
		return id;
	}

}
