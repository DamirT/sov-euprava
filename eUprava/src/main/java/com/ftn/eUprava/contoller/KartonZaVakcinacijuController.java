package com.ftn.eUprava.contoller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ftn.eUprava.model.DozaVakcine;
import com.ftn.eUprava.model.KartonVakcinacije;
import com.ftn.eUprava.model.Korisnik;
import com.ftn.eUprava.model.Pacijent;
import com.ftn.eUprava.model.UlogaKorisnika;
import com.ftn.eUprava.model.Vakcina;
import com.ftn.eUprava.service.KartonService;
import com.ftn.eUprava.service.KorisnikService;
import com.ftn.eUprava.service.VakcinaService;

import ch.qos.logback.core.joran.conditional.ElseAction;

@Controller
@RequestMapping("/vakcinisanje")
public class KartonZaVakcinacijuController {

	private String ULOGOVANI_KORISNIK_KEY = "ulogovani_korisnik";
	
	@Autowired
	private ServletContext servletContext;
	private String bURL;
	
	@Autowired
	private VakcinaService vakcinaService;

	@Autowired
	private KartonService kartonService;
	
	@Autowired
	private KorisnikService korisnikService;
	
	@PostConstruct
	public void init() {
		bURL = servletContext.getContextPath() + "/";
	}
	
	@GetMapping("/prijavaZaVakcinu")
	@ResponseBody
	public String prijavaZaVakcinu(HttpSession session) {
		Korisnik korisnik = (Korisnik) session.getAttribute(ULOGOVANI_KORISNIK_KEY);
		boolean kompletnoVakcinisan = false;
		for (KartonVakcinacije karton : kartonService.findByPacijentId(korisnik.getId())) {
			if(karton.getDozaVakcine().ordinal() == 2 && karton.isVaccinated()) {
				kompletnoVakcinisan = true;
				break;
			} else {
				kompletnoVakcinisan = false;
			}
		}
		
		if(kompletnoVakcinisan) {
			StringBuilder retVal = new StringBuilder();
			
			retVal.append("<!DOCTYPE html>\n"
					+ "<html lang=\"en\">\n"
					+ "<head>\n"
					+ "    <meta charset=\"UTF-8\">\n"
					+ "<base href=\""+bURL+"\">" 
					+ "    \n"
					+ "    <title>Document</title>\n"
					+ "</head>\n"
					+ "<body>\n"
					+ "<a href=\"korisnici/odjava\">Odjava</a></br><br>"
					+ "<div>"
					+ "<caption>Korisnik: " + korisnik.getIme() + " " + korisnik.getPrezime() + " " + korisnik.getJmbg() + " je vakcinisan sa sve tri doze vakcina!</caption></br></br>"
					+ "</div>" 
					+ "    \n"
					+ "</body>\n"
					+ "</html>");
				return retVal.toString();
		} else {
			StringBuilder retVal = new StringBuilder();
			retVal.append("<!DOCTYPE html>\r\n" + 
					"<html>\r\n" + 
						"<head>\r\n" + 
							"<meta charset=\"ISO-8859-1\">\r\n" + 
							"<base href=\""+bURL+"\">" +
							"<title>Insert title here</title>\r\n" + 
						"</head>\r\n" + 
						"<body>\r\n" + 
							"<a href=\"korisnici/odjava\">Odjava</a></br><br>" + 
							"<div>"
								+ "<caption>Prijava za vakcinu korisnika: " + korisnik.getIme() + " " + korisnik.getPrezime() + " " + korisnik.getJmbg() +"</caption></br></br>" + 
							"</div>" +
							"<div>" +
								"<caption>Ako niste vakcinisani nekom dozom vakcine a zelite da je promenite dok cekate, mozete je azurirati selekt polje</caption></br></br>" + 
							"</div>" +
							"	<form method=\"post\" action=\"vakcinisanje/prijavaZaVakcinu\">\r\n" + 
							"		<table>\r\n" + 
							"			<tr><th>Vakcina:</th>" +
							"				<td>" +
							"					<select name=\"idVakcine\">");
					for (Vakcina vakcina : vakcinaService.findAll()) {
						retVal.append("<option value=\"" + vakcina.getId() + "\">"
								+ vakcina.getNaziv() + "</option>");
					}
							
					retVal.append("</select>" + 
							"</td>" + 
							"</tr>\r\n" +
							"			<tr><th>Doza vakcine:</th>" +
							"				<td>" +
							"					<select name=\"dozaVakcine\">");
					List<KartonVakcinacije> kartoni = kartonService.findByPacijentId(korisnik.getId());
					if (kartoni.size() == 0) {
						retVal.append("<option value=\"" + DozaVakcine.getName(DozaVakcine.DOZA_1) + "\">");
						retVal.append(DozaVakcine.getName(DozaVakcine.DOZA_1) + "</option>");
					} else {
						DozaVakcine dozaVakcine = null;
						for (KartonVakcinacije karton : kartonService.findByPacijentId(korisnik.getId())) {
							if (karton.getDatumPrijave() == 0 || !karton.isVaccinated()) {
								dozaVakcine = karton.getDozaVakcine();
							} else if (karton.isVaccinated() && karton.getDozaVakcine().ordinal() == 1) {
								dozaVakcine = DozaVakcine.DOZA_3;
							} else if (karton.isVaccinated() && karton.getDozaVakcine().ordinal() == 0) {
								dozaVakcine = DozaVakcine.DOZA_2;
							}
						}
						if (dozaVakcine != null) {
							retVal.append("<option value=\"" + DozaVakcine.getName(dozaVakcine) + "\">");
							retVal.append(DozaVakcine.getName(dozaVakcine) + "</option>");
						}

					}
							
					retVal.append("</select>" + 
							"</td>" + 
							"</tr>\r\n" +
							"<tr><th></th><td><input type=\"submit\" value=\"Dodaj\" /></td>\r\n" + 
							"</table>\r\n" + 
							"</form>\r\n" +
						"</body>\r\n" + 
					"</html>");
			return retVal.toString();
		}

	}
	
	@PostMapping("/prijavaZaVakcinu")
	public void kartonZaVacinaciju(@RequestParam Long idVakcine, @RequestParam String dozaVakcine,
			HttpSession session, HttpServletResponse response) throws IOException {
		
		Pacijent pacijent = (Pacijent) session.getAttribute(ULOGOVANI_KORISNIK_KEY);
		KartonVakcinacije kartonVakcinacije = null;
		for (KartonVakcinacije kv : kartonService.findByPacijentId(pacijent.getId())) {
			if (DozaVakcine.getName(kv.getDozaVakcine()).equals(dozaVakcine) && kv.getIdVakcine() == idVakcine) {
				return;
			} else if (DozaVakcine.getName(kv.getDozaVakcine()).equals(dozaVakcine) && kv.getIdVakcine() != idVakcine) {
				kartonVakcinacije = kv;
				break;
			}
		}
		if (kartonVakcinacije == null) {
			kartonService.save(new KartonVakcinacije(System.currentTimeMillis(), 0L, idVakcine,
					DozaVakcine.getDozavakcineByName(dozaVakcine), pacijent.getId()));
		} else {
			kartonService.updateVrstuVakcine(kartonVakcinacije, idVakcine);
		}
		response.sendRedirect(bURL + "vakcinisanje/potvrdnaPoruka");
	}
	
	@GetMapping("/potvrdnaPoruka")
	@ResponseBody
	public String potvrdnaPorukaZaPacijenta() {
		return "Uspesno ste se prijavili za vakcinaciju!";
	}
	
	@GetMapping("/kartonZaVakcinaciju")
	@ResponseBody
	public String kartonZaVakcinaciju(HttpSession session) {
		
		Korisnik korisnik = (Korisnik) session.getAttribute(ULOGOVANI_KORISNIK_KEY);
		
		List<Pacijent> pacijenti = new ArrayList<Pacijent>();
		for(Korisnik kor : korisnikService.findAll()) {
			if (kor.getUlogaKorisnika() == UlogaKorisnika.PACIJENT) {
				pacijenti.add((Pacijent) kor);
			}
		} 
		
		List<Pacijent> sviPrijavljeniPacijentiZaVakcinaciju = new ArrayList<Pacijent>();
		for (Pacijent pacijent : pacijenti) {
			List<KartonVakcinacije> kartoniPacijenata = kartonService.findByPacijentId(pacijent.getId());
				for (KartonVakcinacije kv : kartoniPacijenata) {
					if (kv.getIdPacijenta() != null) {
						sviPrijavljeniPacijentiZaVakcinaciju.add((Pacijent) korisnikService.findOne(kv.getIdPacijenta()));
					}
				break;
				}
		}
		
		List<KartonVakcinacije> kartoni = kartonService.findAll();
		
		StringBuilder retVal = new StringBuilder();
		retVal.append("<!DOCTYPE html>\r\n" + 
				"<html>\r\n" + 
				"<head>\r\n" + 
				"	<meta charset=\"UTF-8\">\r\n" + 
	    		"	<base href=\""+bURL+"\">" + 
				"	<title>Prijave</title>\r\n" + 
				"</head>\r\n" + 
				"<body> "+
				"	<ul>\r\n" + 
				"		<a href=\"korisnici/odjava\">Odjava</a></br>" + 
				"	</ul>\r\n" + 
				"		<table>\r\n" + 
				"			<caption>Sve prijave za vakcinaciju</caption>\r\n" + 
				"			<tr>\r\n" + 
				"				<th>Ime</th>\r\n" + 
				"				<th>Prezime</th>\r\n" +
				"				<th>JMBG</th>\r\n" +
				"				<th>Vakcinisi</th>\r\n" +
				"				<th>Datum prijave</th>\r\n" +
				"				<th>Doza</th>\r\n" +
				"				<th>Vakcina</th>\r\n" +
				"				<th></th>\r\n" +
				"				<th></th>\r\n" +
				"			</tr>\r\n");
		for (int index = 0; index < sviPrijavljeniPacijentiZaVakcinaciju.size(); index++) {
			retVal.append("<tr>\r\n" + "<td>" + sviPrijavljeniPacijentiZaVakcinaciju.get(index).getIme() + "</td>\r\n" + "<td>"
					+ sviPrijavljeniPacijentiZaVakcinaciju.get(index).getPrezime() + "</td>\r\n" + "<td>"
					+ sviPrijavljeniPacijentiZaVakcinaciju.get(index).getJmbg() + "</td>\r\n"
					+ "<td>"
					+	"		<form method=\"post\" action=\"vakcinisanje/vakcinisi\">\r\n" + 
					"			<input type=\"hidden\" name=\"idPacijenta\" value=\""+sviPrijavljeniPacijentiZaVakcinaciju.get(index).getId()+"\">\r\n" +
					"			<input type=\"submit\" value=\"Vakcinisi\"></td>\r\n" + 
					"			</form>\r\n" 
						+ "</td>");
			List<KartonVakcinacije> kartoniPacijenata = kartonService.findByPacijentId(sviPrijavljeniPacijentiZaVakcinaciju.get(index).getId());
			for (int i = 0; i < kartoniPacijenata.size(); i++) {
				retVal.append("		<td>" + kartoniPacijenata.get(i).getDatumPrijaveFormated() + "</td>\r\n" + "<td>"
						+ kartoniPacijenata.get(i).getDozaVakcine() + "</td>\r\n");
				retVal.append("		<td>" + vakcinaService.findOne(kartoniPacijenata.get(i).getIdVakcine()).getNaziv()
						+ "<td><a href=\"vakcinisanje/detalji?id="+sviPrijavljeniPacijentiZaVakcinaciju.get(index).getId()+"\">Pogledaj vakcinalni karton</a></td>\r\n"
						+ "</td>\r\n"
						+ "</tr>");
				break;
			}
		}
		retVal.append(
				"		</table>\r\n");
		retVal.append(
				"</body>\r\n"+
				"</html>\r\n");		
		return retVal.toString();			
		
	}
	
	@GetMapping("/detalji")
	@ResponseBody
	public String getPojedinacniKarton(@RequestParam Long id) {
		Pacijent pacijent = (Pacijent) korisnikService.findOne(id);
		
		StringBuilder retVal = new StringBuilder();
		retVal.append("<!DOCTYPE html>\r\n" + 
				"<html>\r\n" + 
				"<head>\r\n" + 
				"	<meta charset=\"UTF-8\">\r\n" + 
	    		"	<base href=\""+bURL+"\">" + 
				"	<title>Karton</title>\r\n" + 
				"</head>\r\n" + 
				"<body> "+
				"	<ul>\r\n" + 
				"		<a href=\"korisnici/odjava\">Odjava</a></br>" + 
				"	</ul>\r\n" + 
				"		<table>\r\n" + 
				"			<h1>Vakcinalni karton - " + pacijent.getIme() + " " + pacijent.getPrezime() + " " + "jmbg: " + pacijent.getJmbg() + "</h1>\r\n" + 
				"			<tr>\r\n" + 
				"				<th>Prijava izvrsena</th>\r\n" + 
				"				<th>Vakcinisan</th>\r\n" + 
				"				<th>Vakcina</th>\r\n" +
				"				<th>Doza</th>\r\n" +
				"			</tr>\r\n");
			List<KartonVakcinacije> kartoniPacijenata = kartonService.findByPacijentId(pacijent.getId());
			for (int i = 0; i < kartoniPacijenata.size(); i++) {
				
				retVal.append("	    <tr><td>" 	+ kartoniPacijenata.get(i).getDatumPrijaveFormated() + "</td>\r\n");
				if(kartoniPacijenata.get(i).isVaccinated()) {
					retVal.append("<td>"	+ kartoniPacijenata.get(i).getDatumVakcinisanjaFormated() + "</td>\r\n");
				} else {
					retVal.append("<td>"	+ " " + "</td>\r\n");
				}
				retVal.append("		<td>"	+ kartoniPacijenata.get(i).getDozaVakcine() + "</td>\r\n"
				+			  "	    <td>"   + vakcinaService.findOne(kartoniPacijenata.get(i).getIdVakcine()).getNaziv() + "</td>\r\n"
								  + "</tr>");
			}
		retVal.append(
				"		</table>\r\n");
		retVal.append(
				"</body>\r\n"+
				"</html>\r\n");		
		return retVal.toString();
	}
	
	@PostMapping("/vakcinisi")
	public void vakcinisiPacijenta(@RequestParam Long idPacijenta, HttpServletResponse response) throws IOException {
		Pacijent pacijent = (Pacijent) korisnikService.findOne(idPacijenta);
		
		KartonVakcinacije kartonVakcinacije = null;
		List<KartonVakcinacije> kartoni = kartonService.findByPacijentId(pacijent.getId());
		for (KartonVakcinacije karton : kartoni) {
			if(karton.getIdPacijenta().equals(pacijent.getId())) {
				kartonVakcinacije = karton;
			}
		}
		
		//if (kartonVakcinacije != null) {
			kartonService.updateDatumVakcinisanja(kartonVakcinacije, System.currentTimeMillis());
		//}
		response.sendRedirect(bURL + "vakcinisanje/kartonZaVakcinaciju"); 
	}
	
}
