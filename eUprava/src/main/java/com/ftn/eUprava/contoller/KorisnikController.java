package com.ftn.eUprava.contoller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ftn.eUprava.model.DozaVakcine;
import com.ftn.eUprava.model.KartonVakcinacije;
import com.ftn.eUprava.model.Korisnik;
import com.ftn.eUprava.model.MedicinskiRadnik;
import com.ftn.eUprava.model.Pacijent;
import com.ftn.eUprava.model.UlogaKorisnika;
import com.ftn.eUprava.model.Vakcina;
import com.ftn.eUprava.service.DataService;
import com.ftn.eUprava.service.KartonService;
import com.ftn.eUprava.service.KorisnikService;
import com.ftn.eUprava.service.VakcinaService;

@Controller
@RequestMapping("/korisnici")
public class KorisnikController {

	private String ULOGOVANI_KORISNIK_KEY = "ulogovani_korisnik";

	@Autowired
	private ServletContext servletContext;
	private String bURL;

	@Autowired
	private VakcinaService vakcinaService;

	@Autowired
	private KartonService kartonService;

	@Autowired
	private KorisnikService korisnikService;

	@PostConstruct
	public void init() {
		bURL = servletContext.getContextPath() + "/";
	}
	
	@GetMapping("/login")
	public String getLogin() {
		return "/index.html";
	}

	@PostMapping("/login")
	@ResponseBody
	public void login(@RequestParam String jmbg, @RequestParam String lozinka, HttpServletResponse response,
			HttpSession session) throws IOException {

		Korisnik logovaniKorisnik = null;

		for (Korisnik korisnik : korisnikService.findAll()) {
			if (korisnik.getJmbg().equals(jmbg) && korisnik.getLozinka().equals(lozinka)) {
				logovaniKorisnik = korisnik;
				break;
			}
		}

		String greska = "";
		if (logovaniKorisnik == null) {
			greska = "Ne postoji trazeni korisnik";
		}

		if (session.getAttribute(ULOGOVANI_KORISNIK_KEY) != null) {
			greska = "korisnik je vec prijavljen - potrebno je da se prethodno odjavite.</br>";
		}

		if (!greska.equals("")) {
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			
			StringBuilder retVal = new StringBuilder();
			retVal.append("<!DOCTYPE html>\r\n" + 
					"<html>\r\n" + 
					"<head>\r\n" + 
					"	<meta charset=\"UTF-8\" />\r\n" + 
					"	<base href=\"/eUprava/\">\r\n" + 
					"	<title>Prijava korisnika na sistem</title>\r\n" + 
					"</head>\r\n" + 
					"<body>\r\n" + 
					"	<h1>Prijava korisnika na sistem</h1><br>\r\n" + 
					"	\r\n" + 
					"	<br/>\r\n");
					
			retVal.append("<div>" + greska + "</div></br>");
			
			retVal.append("	<form method=\"post\" action=\"korisnici/login\">\r\n" + 
					"		<table>\r\n" + 
					"  			<tr>\r\n" + 
					"	    		<th>Email</th>\r\n" + 
					"	    		<td><input type=\"text\" name=\"jmbg\" required/></td>\r\n" + 
					"  			</tr>\r\n" + 
					"  			<tr>\r\n" + 
					"    			<th>Password</th>\r\n" + 
					"   			<td><input type=\"password\" name=\"lozinka\"></td>\r\n" + 
					"  			</tr>\r\n" + 
					"  			<tr>\r\n" + 
					"  				<th></th>\r\n" + 
					"  				<td><input type=\"submit\" value=\"Prijavi se\"></td>\r\n" + 
					"  			</tr>\r\n" + 
					"		</table>\r\n" + 
					"	</form>\r\n" + 
					"</body>\r\n" + 
					"</html>");
			
			out.write(retVal.toString());
			return;
		} else {
			if(logovaniKorisnik.getUlogaKorisnika() == UlogaKorisnika.PACIJENT) {
				System.out.println(logovaniKorisnik.getIme() + " " + logovaniKorisnik.getJmbg());
				session.setAttribute(ULOGOVANI_KORISNIK_KEY, logovaniKorisnik);
				response.sendRedirect(bURL + "vakcinisanje/prijavaZaVakcinu");
			} else {
				System.out.println(logovaniKorisnik.getIme() + " " + logovaniKorisnik.getJmbg());
				session.setAttribute(ULOGOVANI_KORISNIK_KEY, logovaniKorisnik);
				response.sendRedirect(bURL + "vakcinisanje/kartonZaVakcinaciju");
			}
		}
	}
	
	@GetMapping("/odjava")
	public void logout(HttpSession session, HttpServletResponse response) throws IOException {
		session.removeAttribute(ULOGOVANI_KORISNIK_KEY);
		response.sendRedirect(bURL + "korisnici/login");
	}
}
