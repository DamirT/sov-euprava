package com.ftn.eUprava.service;

import java.util.List;

import com.ftn.eUprava.model.DozaVakcine;
import com.ftn.eUprava.model.KartonVakcinacije;
import com.ftn.eUprava.model.Vakcina;

public interface KartonService extends DataService<KartonVakcinacije> {

	public List<KartonVakcinacije> findByPacijentId(Long id);

	public KartonVakcinacije updateDozuVakcine(KartonVakcinacije karton, DozaVakcine dozaVakcine);

	public KartonVakcinacije updateDatumVakcinisanja(KartonVakcinacije karton, Long datum);
	
	public KartonVakcinacije updateVrstuVakcine(KartonVakcinacije karton, Long idVakcine);

}
