package com.ftn.eUprava.service;

import java.util.List;

import com.ftn.eUprava.model.Korisnik;
import com.ftn.eUprava.model.Pacijent;

public interface DataService<T> {

	T findOne(Long id); 
	List<T> findAll(); 
	T save(T data); 
	T update(T data); 
	T delete(Long id);
	
}
