package com.ftn.eUprava.service.impl;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ftn.eUprava.model.Korisnik;
import com.ftn.eUprava.model.MedicinskiRadnik;
import com.ftn.eUprava.model.Pacijent;
import com.ftn.eUprava.model.UlogaKorisnika;
import com.ftn.eUprava.service.DataService;
import com.ftn.eUprava.service.KorisnikService;

@Service
@Qualifier("fajloviKorisnika")
public class KorisnikServiceImpl implements KorisnikService {

	@Value("${korisnik.pathToFile}")
	private String pathToFile;

	Long nextId = 1L;

	private Map<Long, Korisnik> readFromFile() {

		Map<Long, Korisnik> korisnici = new HashMap<>();

		try {
			Path path = Paths.get(pathToFile);
			List<String> lines = Files.readAllLines(path, Charset.forName("UTF-8"));

			for (String line : lines) {
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0) {
					continue;
				}

				String[] tokens = line.split(";");
				Long id = Long.parseLong(tokens[0]);
				String ime = tokens[1];
				String prezime = tokens[2];
				String jmbg = tokens[3];
				String lozinka = tokens[4];
				String uloga = tokens[5];

				Korisnik korisnik;
				if (UlogaKorisnika.values()[Integer.valueOf(uloga)] == UlogaKorisnika.PACIJENT) {
					korisnik = new Pacijent(id, ime, prezime, jmbg, lozinka);
				} else if (UlogaKorisnika.values()[Integer.valueOf(uloga)] == UlogaKorisnika.MEDICINSKI_RADNIK) {
					korisnik = new MedicinskiRadnik(id, ime, prezime, jmbg, lozinka);
				} else {
					continue;
				}

				korisnici.put(id, korisnik);

				if (nextId < id) {
					nextId = id;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return korisnici;
	}


	@Override
	public Korisnik findOne(Long id) {
		Map<Long, Korisnik> korisnici = readFromFile();
		Korisnik korisnik = korisnici.get(id);
		return korisnik;
	}

	@Override
	public List<Korisnik> findAll() {
		Map<Long, Korisnik> korisnici = readFromFile();
		return new ArrayList<>(korisnici.values());
	}

	@Override
	public Korisnik save(Korisnik korisnik) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Korisnik update(Korisnik korisnik) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Korisnik delete(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Korisnik findOneJmbg(String jmbg) {
		Korisnik found = null;
		for (Korisnik korisnik : findAll()) {
			if (korisnik.getJmbg().equals(jmbg)) {
				found = korisnik;
				break;
			}
		}

		return found;
	}

}
