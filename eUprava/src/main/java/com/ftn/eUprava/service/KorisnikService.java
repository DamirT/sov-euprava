package com.ftn.eUprava.service;

import com.ftn.eUprava.model.Korisnik;

public interface KorisnikService extends DataService<Korisnik> {

	public Korisnik findOneJmbg(String jmbg);
	
	
	
}
