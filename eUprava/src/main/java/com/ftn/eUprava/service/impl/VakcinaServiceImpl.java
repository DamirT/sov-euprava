package com.ftn.eUprava.service.impl;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ftn.eUprava.model.KartonVakcinacije;
import com.ftn.eUprava.model.Korisnik;
import com.ftn.eUprava.model.UlogaKorisnika;
import com.ftn.eUprava.model.Vakcina;
import com.ftn.eUprava.service.DataService;
import com.ftn.eUprava.service.VakcinaService;

@Service
@Qualifier("fajloviVakcina")
public class VakcinaServiceImpl implements VakcinaService {

	@Value("${vakcina.pathToFile}")
	private String pathToFile;

	Long nextId = 1L;

	private Map<Long, Vakcina> readFromFile() {

		Map<Long, Vakcina> vakcine = new HashMap<>();

		try {
			Path path = Paths.get(pathToFile);
			List<String> lines = Files.readAllLines(path, Charset.forName("UTF-8"));

			for (String line : lines) {
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0) {
					continue;
				}

				String[] tokens = line.split(";");
				Long id = Long.parseLong(tokens[0]);
				String naziv = tokens[1];

				Vakcina vakcina = new Vakcina(id, naziv);

				vakcine.put(id, vakcina);

				if (nextId < id) {
					nextId = id;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return vakcine;
	}

	@Override
	public Vakcina findOne(Long id) {
		Map<Long, Vakcina> vakcine = readFromFile();
		Vakcina vakcina = vakcine.get(id);
		return vakcina;
	}

	@Override
	public List<Vakcina> findAll() {
		Map<Long, Vakcina> vakcine = readFromFile();
		return new ArrayList<>(vakcine.values());
	}

	@Override
	public Vakcina save(Vakcina data) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Vakcina update(Vakcina data) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Vakcina delete(Long id) {
		// TODO Auto-generated method stub
		return null;
	}


}
