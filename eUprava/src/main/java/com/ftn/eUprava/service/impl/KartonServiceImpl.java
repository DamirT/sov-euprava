package com.ftn.eUprava.service.impl;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ftn.eUprava.model.DozaVakcine;
import com.ftn.eUprava.model.KartonVakcinacije;
import com.ftn.eUprava.model.Korisnik;
import com.ftn.eUprava.model.MedicinskiRadnik;
import com.ftn.eUprava.model.Pacijent;
import com.ftn.eUprava.model.UlogaKorisnika;
import com.ftn.eUprava.model.Vakcina;
import com.ftn.eUprava.service.DataService;
import com.ftn.eUprava.service.KartonService;
import com.ftn.eUprava.service.KorisnikService;
import com.ftn.eUprava.service.VakcinaService;

@Service
@Qualifier("fajloviKartona")
public class KartonServiceImpl implements KartonService {

	@Value("${karton.pathToFile}")
	private String pathToFile;
	
	@Autowired
	private KorisnikService korisnikService;
	
	@Autowired
	private VakcinaService vakcinaService;

	Long nextId = 1L;

	private Map<Long, KartonVakcinacije> readFromFile() {

		Map<Long, KartonVakcinacije> kartoniVakcinacije = new HashMap<>();

		try {
			Path path = Paths.get(pathToFile);
			List<String> lines = Files.readAllLines(path, Charset.forName("UTF-8"));

			for (String line : lines) {
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0) {
					continue;
				}

				String[] tokens = line.split(";");
				Long id = Long.parseLong(tokens[0]);
				Long datumPrijave = Long.parseLong(tokens[1]);
				Long datumVakcinisanja = Long.parseLong(tokens[2]);
				Long idVakcine = Long.parseLong(tokens[3]);
				DozaVakcine dozaVakcine = DozaVakcine.values()[Integer.valueOf(tokens[4])];
				Long idPacijenta = Long.parseLong(tokens[5]);

				kartoniVakcinacije.put(id, new KartonVakcinacije(id, datumPrijave, datumVakcinisanja, idVakcine,
						dozaVakcine, idPacijenta));

				if (nextId < id) {
					nextId = id;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return kartoniVakcinacije;
	}

	private Map<Long, KartonVakcinacije> saveToFile(Map<Long, KartonVakcinacije> kartoniVakcinacije) {

		Map<Long, KartonVakcinacije> ret = new HashMap<>();

		try {
			Path path = Paths.get(pathToFile);
			List<String> lines = new ArrayList<>();

			for (KartonVakcinacije kv : kartoniVakcinacije.values()) {
				String line = "";
				line += String.valueOf(kv.getId()) + ";";
				line += kv.getDatumPrijave() + ";";
				line += kv.getDatumVakcinisanja() + ";";
				line += kv.getIdVakcine() + ";";
				line += kv.getDozaVakcine().ordinal() + ";";
				line += kv.getIdPacijenta();

				lines.add(line);

				ret.put(kv.getId(), kv);
			}
			Files.write(path, lines, Charset.forName("UTF-8"));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Override
	public KartonVakcinacije findOne(Long id) {
		Map<Long, KartonVakcinacije> kartoni = readFromFile();
		KartonVakcinacije karton = kartoni.get(id);
		return karton;
	}

	@Override
	public List<KartonVakcinacije> findAll() {
		Map<Long, KartonVakcinacije> kartoniVakcinacije = readFromFile();
		return new ArrayList<>(kartoniVakcinacije.values());
	}

	@Override
	public KartonVakcinacije save(KartonVakcinacije karton) {
		Map<Long, KartonVakcinacije> kartoni = readFromFile();
		if(karton.getId() == null) {
			karton.setId(++nextId);
		}
		kartoni.put(karton.getId(), karton);
		saveToFile(kartoni);
		return karton;
	}

	@Override
	public KartonVakcinacije update(KartonVakcinacije karton) {
		// TODO Auto-generated method stub
				return null;
	}

	@Override
	public KartonVakcinacije delete(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<KartonVakcinacije> findByPacijentId(Long id) {
		List<KartonVakcinacije> pacijentoviKartoni = new ArrayList<KartonVakcinacije>();
		for (KartonVakcinacije kv : findAll()) {
			if (kv.getIdPacijenta().equals(id)) {
				pacijentoviKartoni.add(kv);
			}
		}
		return pacijentoviKartoni;
	}
	

	@Override
	public KartonVakcinacije updateDozuVakcine(KartonVakcinacije karton, DozaVakcine dozaVakcine) {
		Map<Long, KartonVakcinacije> kartoni = readFromFile();
		if(karton.getId() == null) {
			karton.setId(++nextId);
		}
		karton.setDozaVakcine(dozaVakcine);
		kartoni.put(karton.getId(), karton);
		saveToFile(kartoni);
		return karton;
	}

	@Override
	public KartonVakcinacije updateDatumVakcinisanja(KartonVakcinacije karton, Long datum) {
		Map<Long, KartonVakcinacije> kartoni = readFromFile();
		if(karton.getId() == null) {
			karton.setId(++nextId);
		}
		karton.setDatumVakcinisanja(datum);
		kartoni.put(karton.getId(), karton);
		saveToFile(kartoni);
		return karton;

	}
	
	@Override
	public KartonVakcinacije updateVrstuVakcine(KartonVakcinacije karton, Long idVakcine) {
		Map<Long, KartonVakcinacije> kartoni = readFromFile();
		if(karton.getId() == null) {
			karton.setId(++nextId);
		}
		karton.setIdVakcine(idVakcine);
		kartoni.put(karton.getId(), karton);
		saveToFile(kartoni);
		return karton;
	}

}
